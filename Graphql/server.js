var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
 
let Car=[
	{id:1,name:"Citroen"},
	{id:2,name:"Mercedes"},
	{id:3,name:"BMW"},
	{id:4,name:"Toyota"}

]
let users=[
	{id:1,name:"Jake",email:"Jack@gmail.com"},
	{id:2,name:"Kevin",Age:27}
]

var schema = buildSchema(`
	type User{
	id:ID
	name:String
	email:String
	Age:Int
	}
	type Car{
	id:ID
	name:String
	}

  type Query {
    hello: String
	users: [User!]!
	user(id:ID!):User!
	Cars: [Car!]!
  }
  type Mutation{
	  CreateUser(id:ID,name:String,email:String,age:Int): User!

  }


`);
 

var root = { hello:()=>"Hello",
		 users: () => users,
		   Cars: () => Car,
		   user:(parent,{id})=>users.find(user=>user.id==id),
		   CreateUser:({id,name,email,age})=>{
			   let checkID=users.findIndex(user=> user.id == id)
				if (checkID == -1){
					let newUser = {id:id,name:name,email:email,age:age}
					users.push(newUser)
					return newUser
				}
			}
		 }
	


 
var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));