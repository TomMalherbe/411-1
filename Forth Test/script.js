
var dataExemple;
var download =false;
var Val="Avg";
var Rect;
var data=[[0,0]];
var T2=[];
var request = new XMLHttpRequest();
var Print;
var Print2;
var Print3;
var dataS=[];


//get data from the API
request.open("GET","https://z7l664y1c8.execute-api.us-east-2.amazonaws.com/dev9",true);
request.send();
request.onload = function() {
  dataExemple = JSON.parse(request.response);
  download = true;
}


// Iniatialize Canvas
 canvas = d3.select("body")
  .append("svg")
  .attr("width",1000)
  .attr("height",1000)
  .append("g");

 Print3=canvas.selectAll("rect")


// When button is clicked
function Mode(clicked_id){
  Val=clicked_id;
}

function Tele(clicked_id){

  ColorChange(clicked_id);    //change the color of the button

  if (download==false) {setTimeout(function(){Tele();},1000); // wait for the onload
  }else{
    
    Print2=canvas.append("rect")
      .attr("x",150).attr("y",150).attr("width", 550).attr("height", 360).attr("fill","#eeeeee");  //Set the grey background

    var T = 300;   // X axis 

    data = [[ 0 , 0 ]]; // initialize data
    
    addData( data );
    
    Object.keys(dataExemple).forEach(function(key) { 
      
      var Data=dataExemple[key];
      
      Object.keys(Data).forEach(function(key) {
        
        if(key==clicked_id){             
          
          if (Val=="Avg"){ 
            Rect=Data[key].Avg;
            data.push([Data[key].Avg,T]);
          }
          if (Val=="Min"){
            Rect=Data[key].Min;
            data.push([Data[key].Min,T]);
          }
          if (Val=="Max"){
            Rect=Data[key].Max;
            data.push([Data[key].Max,T]);
          }
          if (Val=="StdDev"){
            Rect=Data[key].StdDev;
            data.push([Data[key].StdDev,T]);
          }
          if (Val=="Count"){
            Rect=Data[key].Count;
            data.push([Data[key].Count,T]);
          }

          Print=canvas.append("text")
          .attr("x",T).attr("y",200).attr("font-size", 25).attr("fill","black").text(Rect);
        }
        if (key=="Data")
        {
          var StartTime = Data[key].substring(11,16);
          var EndTime= Data[key].substring(33,38)

          Print=canvas.append("text")
          .attr("x",T-20).attr("y",460).attr("font-size", 8).attr("fill","black").text(StartTime);

          Print=canvas.append("text")
          .attr("x",T+20).attr("y",460).attr("font-size", 8).attr("fill","black").text(EndTime);  
        }
      })
      T = T + 80;
    })
    Print3.remove();
    addData(data);
  }
}


function ColorChange(clicked_id){
  for (var i=0;i< document.getElementsByClassName("onclick2").length;i++)
  {
    document.getElementsByClassName("onclick2")[i].style.background="#eeeeee";
  }
  document.getElementById(Val).style.background="goldenrod";

  for (var i=0;i< document.getElementsByClassName("onclick").length;i++)
  {
    document.getElementsByClassName("onclick")[i].style.background="#eeeeee";
  }
  document.getElementById(clicked_id).style.background="goldenrod";
}


function addData(data){
  dataS = [];

  for(i=0;i<data.length;i++){
    dataS.push(parseFloat(data[i][0]));
  }

  var y = d3.scaleLinear()
  y.domain([  Math.max.apply(Math, dataS),0]);
  y.range([0,200]);

  var yAxisCall =  d3.axisLeft(y);

  canvas.append("g")
  .attr("class", "y axis")
  .call(yAxisCall)
  .attr("transform", "translate(200,250)");
    
  Print3.data(data)
  .enter()
  .append("rect")
  .attr("x",function(d){return d[1]})
  .attr("y",function(d){return 450-parseFloat(d[0])*200/ Math.max.apply(Math, dataS)})
  .attr("font-size", 25)
  .attr("fill","black")
  .attr("width",15)
  .attr("height",function(d){return parseFloat(d[0])*200/ Math.max.apply(Math, dataS)});

  Print3.data(data)
  .enter()
  .append("rect")
  .attr("x",function(d){return d[1]+2})
  .attr("y",function(d){return 450-parseFloat(d[0])*200/ Math.max.apply(Math, dataS)+2})
  .attr("font-size", 25)
  .attr("fill","white")
  .attr("width",11)
  .attr("height",function(d){return parseFloat(d[0])*200/ Math.max.apply(Math, dataS)-4});

}

