// -------Define var--------

var DataExemple;            // All the Json object
var DataArray;              // Only the data
var DataScore;              // Only the score
var Download=false;         // True if the data fro the Json is send from the API
var canvas;                 // SVG
var Rect;                   // Define rectangles for the data graph
var Rect2;                  // Define rectangles for the score graph
var RectScore=new Array();  // Array data from the DataScore : Datascore 
var texte;                  // Texte

var width  = 1000;          // Width of canvas
var height = 1000;          // Height of canvas
var x = d3.scaleLinear()    // use for X axis 
  .domain([0,100])
  .range([0,500]);
var y = d3.scaleLinear()    // use for y axis 
  .domain([10,0])
  .range([0,500]);
var x2 = d3.scaleLinear()   // use for X axis
  .domain([100,0])
  .range([0,500]);         
var xAxisCall = d3.axisBottom(x) // x axis of data graph
var yAxisCall2 = d3.axisLeft(x2) // x axis of score graph
var yAxisCall =  d3.axisLeft(y)  // y axis of data graph



canvas = d3.select("body")       // Define the Svg
  .append("svg")
  .attr("width",width)
  .attr("height",height)
  .append("g")
  .attr("transform","translate(50,50)")
  .attr("fill","#202020");
canvas.append("g")                // add axis of graph
  .attr("class", "x axis")
  .attr("transform", "translate(0,600)")
  .call(xAxisCall);
canvas.append("g")               // add axis of graph
  .attr("class", "y axis")
  .call(yAxisCall)
  .attr("transform", "translate(0,100)");
  
canvas.append("g")              // add axis of graph
  .attr("class", "x axis")
  .attr("transform", "translate(550,100)")
  .call(yAxisCall2);



// ------- Get Request ---------

var request = new XMLHttpRequest();
request.open("GET","https://p0dwvgjf7k.execute-api.us-east-2.amazonaws.com/Dev2",true);
request.send();
request.onload = function() {
  DataExemple = JSON.parse(request.response); // All the Json file
  DataArray = DataExemple.data;               // data from Json file
  DataScore = DataExemple.scores;             // score from Json file
  Download = true;                            // The data is download
  };


// Fonction for adding the data with d3

function Rectangle(){
  if (Download==false) {        // If the data isn't download, we ask for the function after 1000
    alert("The data is not download yet");
    setTimeout(function(){Rectangle();},1000);    
  }else{                       // Else we start adding the data 
    Rect=canvas.selectAll("rect2")        // Adding the data for the first graph 
      .data(DataArray)
      .enter()
      .append("rect")
      .attr("x", function(d) { return d[0]*5;})
      .attr("height", function(d) {return d[1]*50; })
      .attr("y", function(d) {return 600-d[1]*50;})
      .attr("width", 10)
      .attr("fill","#202020");
                           

    RectScore[0]=DataScore.a;
    RectScore[1]=DataScore.b;
    RectScore[2]=DataScore.c;                                
    Rect2=canvas.selectAll("rect3")     // Adding the data for the second graph 
      .data(RectScore)
      .enter()
      .append("rect")
      .attr("x",function(d,i){return 600+100*i;})
      .attr("height",function(d){return d*500/100;})
      .attr("y", function(d){return 600-d*500/100;})
      .attr("width", 60)
      .attr("fill","#202020");
    
    var texte2  =canvas.append("text")      //Adding texte in the Canvas, I think it will be easier with class on html
      .attr("x",600+31)
      .attr("y",620)
      .attr("dy", ".35em")
      .attr("font-size", 60)
      .text(  "a" )
      .attr("font-family","bold")
      .attr("fill","#202020");
    var texte3  =canvas.append("text")
      .attr("x",700+31)
      .attr("y",620)
      .attr("dy", ".35em")
      .attr("font-size", 40)
      .text(  "b" )
      .attr("font-family","bold")
      .attr("fill","#202020");
    var texte4  =canvas.append("text")
      .attr("x",800+31)
      .attr("y",620)
      .attr("dy", ".35em")
      .attr("font-size", 40)
      .text(  "c" )
      .attr("font-family","bold")
      .attr("fill","#202020"); 
    var texte5  =canvas.append("text")
      .attr("x",35)
      .attr("y",120)
      .attr("dy", ".35em")
      .attr("font-size", 40)
      .text(  "data" )
      .attr("font-family","bold")
      .attr("fill","#202020");
    var texte6  =canvas.append("text")
      .attr("x",600)
      .attr("y",120)
      .attr("dy", ".35em")
      .attr("font-size", 40)
      .text(  "scores" )
      .attr("font-family","bold")
      .attr("fill","#202020");
              
    Rect.on("mouseover", function(d) {          //define the function to write the value of data if the mouse in on the data graph
      d3.select(this)
        .style('fill', 'goldenrod');                  
      texte=canvas.append("text")
        .attr("x",this.x.baseVal.value-30+this.width.baseVal.value/2+11)
        .attr("y",this.y.baseVal.value-45+10)
        .attr("dy", ".35em")
        .attr("font-size", 20)
        .text(  this.x.baseVal.value/5 + "|" + this.height.baseVal.value/50)
        .attr("font-family","bold")
        .attr("fill","goldenrod")
        .transition()
        .duration(1000);
    });
    Rect.on('mouseout', function() {
      d3.select(this)
        .transition()
        .duration(1000)
        .style('fill', '#202020');
      texte.transition()
        .remove()                            
    });

    Rect2.on("mouseover", function(d) {   //define the function to write the value of data if the mouse in on the scores graph 
      d3.select(this)
        .style('fill', 'goldenrod');                                                             
      texte=canvas.append("text")
        .attr("x",this.x.baseVal.value-30+this.width.baseVal.value/2+11)
        .attr("y",this.y.baseVal.value-45+10)
        .attr("dy", ".35em")
        .attr("font-size", 40)
        .text(  this.height.baseVal.value/5 )
        .attr("font-family","bold")
        .attr("fill","goldenrod")
        .transition()
        .duration(1000);
    })
    Rect2.on('mouseout', function() {
      d3.select(this)
      .transition()
      .duration(1000)
      .style('fill', '#202020');
      texte.transition()
      .remove();
    });
  }
}

Rectangle();
