//Definition of Variable

var download= false;
var dataExemple;
var width  = 1000;
var height = 1000;
var request = new XMLHttpRequest();


// GET request

request.open("GET","  https://p0dwvgjf7k.execute-api.us-east-2.amazonaws.com/Dev3",true);
request.send();
request.onload = function() {
  dataExemple = JSON.parse(request.response);
  download = true;
}

canvas1 = d3.select("body")
  .append("svg")
  .attr("width",width)
  .attr("height",height)
  .append("g");


// We add the name of the cities and their localisation on the map .
//Maybe it will be better to define all the text in one and all the circle too (with a .data and with function(d) when something change)
var title  =canvas1.append("text").attr("x",20).attr("y",20).attr("dy", ".35em").attr("font-size", 40).attr("font-family","bold").attr("fill","black");

var Riga=canvas1.append("circle").attr("cx",255).attr("cy",142).attr("r",5).attr("fill","goldenrod");

var NewYork=canvas1.append("circle").attr("cx",120).attr("cy",168).attr("r",5).attr("fill","goldenrod");

var Paris=canvas1.append("circle").attr("cx",231).attr("cy",153).attr("r",5).attr("fill","goldenrod");

var CapeTown=canvas1.append("circle").attr("cx",255).attr("cy",288).attr("r",5).attr("fill","goldenrod");

var Rio=canvas1.append("circle").attr("cx",156).attr("cy",270).attr("r",5).attr("fill","goldenrod");

var Tokyo=canvas1.append("circle").attr("cx",430).attr("cy",168).attr("r",5).attr("fill","goldenrod");

var Sydney=canvas1.append("circle").attr("cx",448).attr("cy",288).attr("r",5).attr("fill","goldenrod");

var NewYork2=canvas1.append("text")
.attr("x",30).attr("y",70).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("NewYork");

var Paris2=canvas1.append("text")
.attr("x",160).attr("y",70).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Paris");

var Riga2=canvas1.append("text").attr("x",280).attr("y",70).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Riga");

var Tokyo2=canvas1.append("text").attr("x",400).attr("y",70).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Tokyo");

var Rio2=canvas1.append("text").attr("x",110).attr("y",95).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Rio");

var CapeTown2=canvas1.append("text").attr("x",200).attr("y",95).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("CapeTown");

var Sydney2=canvas1.append("text").attr("x",340).attr("y",95).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Sydney");

var Europe=canvas1.append("text").attr("x",240).attr("y",595).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Europe");

var Population=canvas1.append("text").attr("x",380).attr("y",550).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("0");

var Temp=canvas1.append("text").attr("x",30).attr("y",380).attr("dy", ".25em").attr("font-size", 25).attr("font-family","bold").attr("fill","goldenrod").text("Mean Temperature");

 
function Tele(){
  if (download==false) { // Wait for the Json file
    alert("The data is not download yet");
    setTimeout(function(){Tele();},1000);
  }else{
    title.text(dataExemple.title)

    NewYork2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Newyork.meanTemperature)
      .attr("y",600-8*dataExemple.Newyork.meanTemperature);
      NewYork.transition()
      .attr("r",10)
      .attr("fill","black");
      NewYork2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.Newyork.Europe);
      Popul(dataExemple.Newyork.Population);
      })
    
    NewYork2.on("mouseout",function(){
      NewYork.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      NewYork2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      })  


    Paris2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Paris.meanTemperature)
      .attr("y",600-8*dataExemple.Paris.meanTemperature);
      Paris.transition()
      .attr("r",10)
      .attr("fill","black");
      Paris2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.Paris.Europe);
      Popul(dataExemple.Paris.Population);
      })
    
    Paris2.on("mouseout",function(){
      Paris.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      Paris2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      })  

    Riga2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Riga.meanTemperature)
      .attr("y",600-8*dataExemple.Riga.meanTemperature);
      Riga.transition()
      .attr("r",10)
      .attr("fill","black");
      Riga2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.Riga.Europe);
      Popul(dataExemple.Riga.Population);
      })
    
    Riga2.on("mouseout",function(){
      Riga.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      Riga2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      }) 

    Tokyo2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Tokyo.meanTemperature)
      .attr("y",600-8*dataExemple.Tokyo.meanTemperature);
      Tokyo.transition()
      .attr("r",10)
      .attr("fill","black");
      Tokyo2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.Tokyo.Europe);
      Popul(dataExemple.Tokyo.Population);
      })
    
    Tokyo2.on("mouseout",function(){
      Tokyo.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      Tokyo2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      }) 

    Rio2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Rio.meanTemperature)
      .attr("y",600-8*dataExemple.Rio.meanTemperature);
      Rio.transition()
      .attr("r",10)
      .attr("fill","black");
      Rio2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.Rio.Europe);
      Popul(dataExemple.Rio.Population);
      })
    
    Rio2.on("mouseout",function(){
      Rio.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      Rio2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      }) 

    CapeTown2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.CapTown.meanTemperature)
      .attr("y",600-8*dataExemple.CapTown.meanTemperature);
      CapeTown.transition()
      .attr("r",10)
      .attr("fill","black");
      CapeTown2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      InEurope(dataExemple.CapTown.Europe);
      Popul(dataExemple.CapTown.Population);
      })
    
    CapeTown2.on("mouseout",function(){
      CapeTown.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      CapeTown2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      }) 

    Sydney2.on("mouseover",function(){
      Rectangle.transition()
      .attr("height",8*dataExemple.Syndey.meanTemperature)
      .attr("y",600-8*dataExemple.Syndey.meanTemperature);
      Sydney.transition()
      .attr("r",10)
      .attr("fill","black");
      Sydney2.transition()
      .attr("fill","black")
      .attr("font-size","30");
      if (dataExemple.Syndey.Europe){
        Europe.transition()
        .attr("fill","goldenrod");
        document.getElementById("Europe").style.display="inline";

      }else{
        Europe.transition()
        .attr("fill","transparent");
        document.getElementById("Europe").style.display="none";
      }
      Popul(dataExemple.Syndey.Population) 
      })
    
    Sydney2.on("mouseout",function(){
      Sydney.transition()
      .attr("r",5)
      .attr("fill","goldenrod");
      Sydney2.transition()
      .attr("fill","goldenrod")
      .attr("font-size","25");
      }) 
      
  }
}
Tele();



var y = d3.scaleLinear()
  .domain([25,0])
  .range([0,200]);
var yAxisCall =  d3.axisLeft(y);

canvas1.append("g")
  .attr("class", "y axis")
  .call(yAxisCall)
  .attr("transform", "translate(80,400)");

var Rectangle=canvas1.append("rect")
.attr("height",5)
.attr("width",30)
.attr("x", 100)
.attr("y",600-5)
.attr("fill","goldenrod");


function InEurope(P){ // add the image Europe if the cities is say in europe on the json file
 if (P){
    Europe.transition()
    .attr("fill","goldenrod");
    document.getElementById("Europe").style.display="inline";

  }else{
    Europe.transition()
    .attr("fill","transparent");
    document.getElementById("Europe").style.display="none";
  };
};
var Loop=0;

function Popul(Number){// Write the population of the city with an animation
    
  if (Loop==0){
    Population.transition()
    .duration(100)
    .text("Population :"+10+"")
    Loop=1;
    setTimeout(function(){Popul(Number);},100);
  }
  
  else if (Loop==1){
    Population.transition()
    .duration(100)
    .text("Population :"+100+"")
    Loop=2;
    setTimeout(function(){Popul(Number);},100);
  }
  
  else if (Loop==2){
    Population.transition()
    .duration(100)
    .text("Population"+1000+"")
    Loop=3;
    setTimeout(function(){Popul(Number);},100);
  }
  else if (Loop==3){
    Population.transition()
    .duration(100)
    .text("Population :"+10000+"")
    Loop=4;
    setTimeout(function(){Popul(Number);},100);
  }

  else if (Loop==4){
    Population.transition()
    .duration(100)
    .text("Population :"+100000+"")
    Loop=5;
    setTimeout(function(){Popul(Number);},100);
    }
  
  else if (Loop==5){
    Population.transition()
    .duration(100)
    .text("Population :"+Number+"")
    Loop=0;
  }  
  else{
    Loop=0;
  }    
         
}
